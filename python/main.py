from bots import get_bot
import argparse
import socket

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Start a bot.")
    parser.add_argument('host', help="Hostname of the raceserver")
    parser.add_argument('port', type=int, help="Port of the raceserver")
    parser.add_argument('bot_name', help="The name of the bot")
    parser.add_argument('bot_key', help="The key of the bot")

    args = parser.parse_args()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((args.host, args.port))
    bot = get_bot(args.bot_name)
    bot(s, args.bot_name, args.bot_key).run()