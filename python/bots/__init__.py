def get_bot(name):
    try:
        module = __import__('bots.%sbot' % name.lower(), fromlist=['%sBot' % name.lower().title()])
        return getattr(module, '%sBot' % name.title())
    except:
        raise Exception("Invalid bot!")
