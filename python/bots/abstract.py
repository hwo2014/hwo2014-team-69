import json
import re

class AbstractBot(object):

    def __init__(self, socket, name, key):
        self._socket = socket
        self._name = name
        self._key = key
        self._cur_tick = 0

    def _msg(self, msg_type, data):
        self._send(json.dumps({"msgType": msg_type, "data": data}))

    def _send(self, msg):
        self._socket.send(msg + "\n")

    def join(self):
        self._msg("join", {
            "name": self._name,
            "key": self._key})

    def throttle(self, throttle):
        self._msg("throttle", throttle)

    def ping(self):
        self._msg("ping", {})

    def switch_lane(self, direction):
        if direction in ("Right", "Left"):
            self._msg("switchLane", direction)

    def run(self):
        self.join()
        self.msg_loop()

    def log(self, msg):
        print "Tick %5d: %s" % (self._cur_tick, msg)

    def on_your_car(self, data):
        self._id = data

    def msg_loop(self):
        socket_file = self._socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            if 'gameTick' in msg:
                self._cur_tick = msg['gameTick']

            func_name = 'on_' + re.sub(r'[A-Z]', lambda m: '_' + m.group(0).lower(), msg['msgType'])

            if hasattr(self, func_name) and callable(getattr(self, func_name)):
                getattr(self, func_name)(msg['data'])
            else:
                self.log(msg['msgType'])
                self.ping()
            line = socket_file.readline()